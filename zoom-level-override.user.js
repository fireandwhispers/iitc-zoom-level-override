// ==UserScript==
// @name         Zoom Level Override
// @id           zoomLevelOverride
// @namespace    https://gitlab.com/fireandwhispers/iitc-zoom-level-override
// @updateURL    https://gitlab.com/fireandwhispers/iitc-zoom-level-override/raw/master/zoom-level-override.user.js
// @downloadURL  https://gitlab.com/fireandwhispers/iitc-zoom-level-override/raw/master/zoom-level-override.user.js
// @category     Misc
// @version      0.2.1
// @description  Sets a persistent zoom level.
// @author       fireandwhispers
// @include      https://*.ingress.com/intel*
// @include      http://*.ingress.com/intel*
// @match        https://*.ingress.com/intel*
// @match        http://*.ingress.com/intel*
// @grant        none
// ==/UserScript==

function wrapper(plugin_info) {
  if(typeof window.plugin !== 'function') window.plugin = function() {};

  plugin_info.buildName = 'babyGoats';
  plugin_info.dateTimeVersion = '20170803140800';
  plugin_info.pluginId = 'zoomLevelOverride';

  window.plugin.zoomLevelOverride = function () {};
  var self = window.plugin.zoomLevelOverride;
  self.data = {
    enabled: false,
    safeMode: true,
    zLevel: 15
  };

  self.saveStorage = function () {
    localStorage.zoomLevelOverride = JSON.stringify(self.data);
  };

  self.loadStorage = function () {
    if (localStorage.zoomLevelOverride)
      self.data = JSON.parse(localStorage.zoomLevelOverride);
  };

  self.changeSettings = function () {
    if (document.getElementById('zoomLevelOverride_toggle')) {
      self.data.enabled = document.getElementById('zoomLevelOverride_toggle').checked;
      self.data.safeMode = document.getElementById('zoomLevelOverride_safeMode').checked;
      self.data.zLevel = parseInt(document.getElementById('zoomLevelOverride_zLevel').value);
      self.saveStorage();
    }
  };

  self.zoomOptions = function () {
      var html = "";
      for (var level in Z_LEVELS) {
        if (Z_LEVELS.hasOwnProperty(level)) {
          html += `<option value="${Z_LEVELS[level]}" ${Z_LEVELS[level] == self.data.zLevel ? "selected" : ""}>${level}</option>`;
        }
      }
      return html;
  };

  self.settings = function () {
    window.dialog({
      id: 'plugin-zoomLevelOverride',
      title: 'Zoom Level Override',
      html: `<h3>Enabled:</h3><p><input type="checkbox" ${self.data.enabled ? "checked" : ""} id="zoomLevelOverride_toggle"></p><hr>` +
        `<h3>Safe Mode:</h3><p>In safe mode, this plugin will not take effect when zoomed out to large areas.</p>` +
        `<p><input type="checkbox" ${self.data.safeMode ? "checked" : ""} id="zoomLevelOverride_safeMode"></p><hr>` +
        `<h3>Zoom Level:</h3><p><select id="zoomLevelOverride_zLevel">` + self.zoomOptions() + `</select></p>`
    });
    document.getElementById('zoomLevelOverride_toggle')
      .addEventListener('change', self.changeSettings);
    document.getElementById('zoomLevelOverride_safeMode')
      .addEventListener('change', self.changeSettings);
    document.getElementById('zoomLevelOverride_zLevel')
      .addEventListener('change', self.changeSettings);
  };

  var Z_LEVELS = {
    "Links >200km": 4,
    "Links >60km": 6,
    "Links >10km": 7,
    "Links >5km": 8,
    "Links >2.5km": 10,
    "Links >800m": 11,
    "Links >300m": 12,
    "All Links": 13,
    "All Portals": 15
  };

  var z_super = window.getDataZoomForMapZoom;
  window.getDataZoomForMapZoom = function() {
    var original_zoom = z_super.apply(this, arguments);
    if (self.data.enabled) {
      if (original_zoom < Z_LEVELS["All Links"] && self.data.safeMode) {
        return original_zoom;
      } else {
        return self.data.zLevel;
      }
    } else {
      return original_zoom;
    }
  };

  function setup() {
    self.loadStorage();
    document.getElementById('toolbox')
      .innerHTML += '<a onclick="window.plugin.zoomLevelOverride.settings();return false;">ZoomLevel</a>';
  }

  setup.info = plugin_info;

  if (!window.bootPlugins) window.bootPlugins = [];
  window.bootPlugins.push(setup);
  if (window.iitcLoaded && typeof setup === 'function') setup();
}

var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) {
  info.script = {
    version: GM_info.script.version,
    name: GM_info.script.name,
    description: GM_info.script.description
  };
}
var textContent = document.createTextNode('(' + wrapper + ')(' + JSON.stringify(info) + ')');
script.appendChild(textContent);
(document.body || document.head || document.documentElement)
.appendChild(script);
